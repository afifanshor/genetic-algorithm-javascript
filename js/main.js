var population
var lifespan = 200
var visualLifespan
var count = 0
var target
var generation = 1
function setup() {
    createCanvas(400, 300)

    population = new Population()
    visualLifespan = createP()
    target = createVector(width/2, 50)
}

function draw() {
    background(42, 49, 57)
    population.run()
    visualLifespan.html(count)
    count++

    if (count == lifespan) {
        population.evaluate()
        population.selection()
        count = 0
        generation++
        document.getElementById("GenerationText").innerHTML="Generation : "+generation
    }

    ellipse(target.x, target.y, 16, 16)
}