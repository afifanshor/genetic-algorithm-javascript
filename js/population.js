// population.js
function Population() {
    this.sticks = []
    this.populationSize = 25
    this.pool = []

    for(var i = 0; i < this.populationSize; i++) {
        this.sticks[i] = new Stick()
    }

    this.evaluate = function() {
        var maxFitness = 0
        for(var i = 0; i < this.populationSize; i++) {
            this.sticks[i].calculateFitness()
            if (this.sticks[i].fitness > maxFitness) {
                maxFitness = this.sticks[i].fitness
            }
        }

        for(var i = 0; i < this.populationSize; i++) {          
            this.sticks[i].fitness /= maxFitness // normalize
        }

        this.pool = []
        for(var i = 0; i < this.populationSize; i++) {          
            var n = this.sticks[i].fitness * 100
            for (var j = 0; j < n; j++) {
                this.pool.push(this.sticks[i])
            }
        }
    }

    this.selection = function() {
        var newSticks = [];
        for (var i = 0; i < this.sticks.length; i++) {

          var parentA = random(this.pool).dna;
          var parentB = random(this.pool).dna;
          
          var child = parentA.crossover(parentB);
          //child.mutation();
          
          newSticks[i] = new Stick(child);
        }
        
        this.sticks = newSticks;
    }

    this.run = function() {
        for(var i = 0; i < this.populationSize; i++) {
            this.sticks[i].update()
            this.sticks[i].show()
        }
    }
}